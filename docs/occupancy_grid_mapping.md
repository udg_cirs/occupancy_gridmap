# Occupancy Grid Mapping

In this lab a 2D occupancy grid mapping will be implemented. The application has to be able to interact with other ROS nodes. Sensor measurements (i.e., $z_i$) will be received from a `sensor_msgs/LaserScan` message while position measures (i.e., $x_i$) will be obtained from the scanning sensor transformation (*tf*) with respect to the fixed frame. The grid map must be updated using the *Log Odds* equations. The resulting occupancy grid map must be published using a `nav_msgs/OccupancyGrid` message. The Octomap server, that is the standard occupancy mapping tool in ROS, will be also tested.

## Pre lab

You need to prepare the following BEFORE going to the first lab session. The professor in the lab will ask you to show them all these items.

### Package and launch file

In this lab we will use the Gazebo simulator and an instance of the turtlebot3 robot. Be sure you have all these packages installed in your computer:

```bash
$ sudo apt update
$ sudo apt install ros-noetic-turtlebot3-msgs
$ sudo apt install ros-noetic-turtlebot3
$ sudo apt install ros-noetic-turtlebot3-teleop
$ sudo apt install ros-noetic-turtlebot3-gazebo
$ sudo apt install ros-noetic-turtlebot3-simulations
```

There are several models of turtlebot3 and we will use the `burger` model. To set it up, add the following line in the `~/.bashrc` file:

```bash
export TURTLEBOT3_MODEL=burger
````

<div style="text-align: center">
    <img src='./imgs/TurtleBot3_Burger.jpg' width='250'/>
</div>

The turtlebot3 burger has a 2D lidar installed on top named [RPlidar](http://wiki.ros.org/rplidar). 
This sensor produces `sensor_msgs/LaserScan` messages. The goal of this lab is to use the lidar to create a 2D occupancy grid map combining the lidar scan with the turtlebot3 odometry data.

Understand the `sensor_msgs/LaserScan` and the `nav_msgs/OccupancyGrid` messages and prepare a catkin package named `mapping_GROUP_NAME` and add a launch file named `turtlebot3.launch` that includes the following elements: 

```xml
<launch>
    <include file="$(find turtlebot3_gazebo)/launch/turtlebot3_house.launch"/>  
    <node pkg="rviz" type="rviz" name="rviz"/>
    <node pkg="robot_state_publisher" type="robot_state_publisher" name="robot_state_publisher"/>
    <include file="$(find turtlebot3_teleop)/launch/turtlebot3_teleop_key.launch" />
</launch>
```
> *What is each line used for?* Describe it using a comment in the same launch file.

Check if you can execute the launch file in your computer. You should see something like the following figure:

<div style="text-align: center">
    <img src='./imgs/gazebo_home.png' width='600'/>
</div>

*Note: If you have problems with the `turtlebot3_house` environment use the `turtlebot3_stage4` environment instead.*

### Bresenham algorithm

Implement the Bresenham's Line Algorithm that we have presented at class. Use the following interface:

```python
def bresenham_line(start, end)
    ...
    return points
```

Where `start` and `end` are 2 2D tuples or lists and `points` is a list containing all 2D points from `start` to `end`. Because points arte in fact cell indexes, they should be integers. 

Put the code in a separate file named `bresenham.py` and add it to your package.

## Lab Part 1: Grid Map Server

To fully implement a Grid Map Server, in addition to the `bresenham.py` file, you will need to create two more code files.

### `gridmap.py`: 

A python file that defines a `class` with the following interface:

```python
 # Initialize gridmap class
    def __init__(self, center, cell_size=0.1, map_size=10):
        self.cell_size = cell_size
        self.grid = np.zeros((int(map_size/cell_size), int(map_size/cell_size)))
        self.origin = np.array(center) - np.array([map_size, map_size])/2

    # return map
    def get_map(self):
        return self.grid
    
    # return origin
    def get_origin(self):
        return self.origin
    
    # Transform position wrt frame origin to cell indexes
    def __position_to_cell__(self, position):
        ...

    # Update cell uv with probability p
    def __update_cell__(self, uv, p):
        ...

    # Update all cells traversed by a ray
    def add_ray(self, start, angle, rng, p):
        ...
```

Where:
* The clamping values for each cell are defined as `GridMap.LMAX` and `GridMap.LMIN`.
* The object is initialized using 3 parameters:
    * `center`: the center position ($x$, $y$) of the grid with respect to the `fixed_frame`.
    * `map_size`: the size of the grid map in meters *(width x width*).
    * `cell_size`: the size of each cell in meters (*cell_size x cell_size*).
* To update a single cell (i.e., `update_cell` function) you need to specify the cell index to be updated (i.e., `uv`) and the `p` (i.e., probability of $x_i$ to be occupied). Be sure that $0 < p(x_i) < 1$ always holds.
* A function that given the sensor position (i.e., `start`), the angle the ray is pointing (i.e., `angle`), the `range` of the measured ray, and `p`, computes all the cell indexes between `start` and `end` using the *bresenham* algorithm defined in the pre-lab and updates the value of all cells between the `start` of the ray and the $end - 1$ of the ray with $1 - p$ and the cell at the end of the ray with $p$.
* `__position_to_cell__` is an auxiliary method that transform a position with respect to the `fixed_frame` to a cell index (`uv`) in the grid map. This function must be called from the `add_ray` function to transform the `start` and `end` of the ray to cell indexes before using the `bresenham_line` function.

$$
cell = \frac{position - map_{origin}}{map_{cell\_size}}, \text{ as integers.}
$$

To update a cell ($l_{i}$) use the *logodds* update equation with clamping explained at class.

$$
l_{t,i} = l_{t-1, i} + \text{inv\_sensor\_model}(m_i, x_t, z_t)\\
\text{clamp } l_{t,i} \text{ between LMAX and LMIN}
$$

being the `inv_sensor_model`:

$$
\text{inv\_sensor\_model}(m_i, x_t, z_t) = \log\left(\frac{p(m_i|z_t,x_t)}{1-p(m_i|z_t, x_t)}\right)
$$

and clipping the resulting value of $l_{t,i}$ between `GridMap.LMAX` and `GridMap.LMIN`.

> Use a `numpy` 2D array to keep the probability of each cell to be occupied or free using log odds. Notice that the angle of each ray depends not only on the angle of the ray with respect of the sensor but also on the orientation of the sensor with respect to the `fixed_frame`.

### `gridmap_server_node.py`

This is another file that implements a ROS node. It takes as input:
* the mapping sensor `topic`, that has to be of type `sensor_msgs/LaserScan`,
* the `fixed_frame` identifier,
* the grid map `map_width`, and
* the grid map `cell_size`.

The node has to:

* Subscribes to the mapping sensor topic.
* Get the sensor pose with respect of the `fixed_frame` identifier (you can use a `tf2 TransformListener` for this purpose).
* Each time a `LaserScan` message is received, call the `GridMap.add_ray` method implemented before for each ray included in the message.
* Take the grid map structure in the `GridMap` object and publish it using a `nav_msgs/OccupancyGrid` message.

Run the launch file given in the pre-lab and the `gridmap_server_node.py` and check with the `RViz` that you can map the turtlebot3 environment while you teleoperate the robot through it.

### Dynamic grid size [extra part]

The `nparray` containing the `grid` map structure has a fixed size. The problem with this is that when we try to update a cell that is outside this grid the system crashes. Modify the grid size dynamically in such a way that when a measure outside the initial grid is added, the grid expands in that direction. 

If you do not implement this extra part, initialize the grid large enough so that it does not collapse.

This extra part will give up to 1 additional points (i.e., you can reach an 11 over 10 but I'll clip the grade to 10).

<div style="page-break-after: always;"></div>

## Lab Part 2: Octomap Server

The Octomap server is a popular occupancy mapping library integrated in ROS. To install it in ROS Noetic do:

```bash
    sudo apt install ros-noetic-octomap*
    sudo apt install octovis
```

You can find information about how to use it in its [wiki](http://wiki.ros.org/octomap_server).

To test the Ocotmap server with the turtlebot3 robot we need to transform the `sensor_msgs/LaserScan` messages generated by the turtlebot 2D lidar into `sensor_msgs/PointCloud2` messages. You can transform from `LaserScan` to `PointCLoud2` using the `LaserGeometry` utility (see an [example here](https://programmerclick.com/article/2916699739/)). For this purpose, you can use the node named `laser_scan_to_point_cloud_node.py` available in this package.

### Testing the Octomap server

To test the octomap server you have to:

* Run the `laser_scan_to_point_cloud_node.py` that subscribes to a `sensor_msgs/LaserScan`, transforms it into a `sensor_msgs/PointCloud2` and publishes them.
* Create a launch file named `octomap.launch` that starts this node, the Octomap server, and the `turtlebot3.launch` created in the pre-lab.
* Teleoperate the turtlebot3 for the simulated environment using the `turtlebot3_teleop_keyboard` node and build a map of it. Visualize the map using the `rviz` tool. 
* Save the generated map and visualize it using the following commands:

```bash
    rosrun octomap_server octomap_saver NAME_OF_THE_MAP.bt
    octovis NAME_OF_THE_MAP.bt
```

> In `octovis` you can *play* with the map resolution. What happens if you press the 'F' key? 

You should obtain something similar to the following figure:

<table>
    <tr>
        <td><img src="./imgs/rviz_octomap.png" width=400></td>
        <td><img src="./imgs/octovis.png" width=400></td>
    </tr>
</table>

<div style="page-break-after: always;"></div>

## Deliverable

Deliver ONLY a `ZIP` file containing the package you have created. Be sure to use this structure:

```
occupancy_gridmap
├── CMakeLists.txt
├── README.md
├── data
│   └── map.bt
├── docs
│   ├── imgs
│   │   ├── TurtleBot3_Burger.jpg
│   │   └── ...
│   └── occupancy_grid_mapping.md
├── launch
│   ├── octomap.launch
│   └── turtlebot3_sim.launch
├── package.xml
└── src
    ├── gridmap_server_node.py
    ├── laser_scan_to_point_cloud_node.py
    ├── occupancy_gridmap
    │   ├── __init__.py
    │   ├── bresenham.py
    │   └── gridmap.py
    └── test.ipynb
```

where:
* `CMakeLists.txt` is the package CMake file.	
* `octomap.launch` is the launch file that starts the `octomap_server` node and the `laser_scan_to_point_cloud.py` node.	
* `turtlebot3.launch` is the launch file that starts the turtlebot3 simulation.
* `map.bt` is the binary file generated by the `octomap_saver` in which a map of *home* or *stage4* environments are stored.
* `package.xml` contains the information about the package.
* `README.md` It has to contain at least:
    * Name of the people in the group.
    * Any particular information I need to know in order to run the package. 
    * A screenshoot of an execution of your Grid Map server, the Octomap Server and the Octovis with a small explanation. Store the images in the `media` folder.
    * Any comment you want to do about this lab.
* `gridmap_server_node.py` The ROS node that implements the GridMap server.
* `laser_scan_to_point_cloud_node.py`The ROS node that transform `laser_scans` into `point_clouds`.
* `bresenham.py` is a utility function to compute the ray path between two points.
* `gridmap.py` is the GridMap class.
* `test.ipynb` is a notebook that contains some tests scripts for the bresenham function and the Gridmap class.
