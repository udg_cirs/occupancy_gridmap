# On-line occupancy gridmap

**This project belongs to the Universitat de Girona. It is forbidden to publish this project or any derivative work in any public repository.**

<div style="text-align: center">
    <a href="https://udg.edu"> 
        <img src="./docs/imgs/udg_logo.png" width=120px/>
    </a>
</div>

## How to complete this lab

Download this repossitory in your *catkin workspace* and follow the instructions provided in the lab guide in the [`docs`](./docs/occupancy_grid_mapping.md) folder.

## Deliverable:

Only this repoossitory must be delivered as a `ZIP` file. Include in this `README.md` file the following information:

* Members of the goup:
* How to run the occupancy gridmap server
* Problems found:
* Examples of use: *Include images (put them in `docs/imgs` folder) or links to videos.*
* Conclusions:
* Any additional part that you have implemented
* Anything else ...
