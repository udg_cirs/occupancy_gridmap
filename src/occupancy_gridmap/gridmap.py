import numpy as np
from occupancy_gridmap.bresenham import bresenham_line


class GridMap:
    LMAX = 6.91 
    LMIN = -6.91


    # Initialize gridmap class
    def __init__(self, center, cell_size=0.1, map_size=10):
        self.cell_size = cell_size
        self.grid = np.zeros((int(map_size/cell_size), int(map_size/cell_size)))
        self.origin = np.array(center) - np.array([map_size, map_size])/2


    # return map
    def get_map(self):
        return self.grid


    # return origin
    def get_origin(self):
        return self.origin
    

    # Transform position wrt frame origin to cell indexes
    def __position_to_cell__(self, position):
        ...


    # Update cell uv with probability p
    def __update_cell__(self, uv, p):
        # Check that probability p is between 0 and 1 but not 0 or 1
        ...

        # Compute inverse sensor model
        ...

        # Update cell and saturate between GridMap.LMIN and GridMap.LMAX 
        ...


    # Update all cells traversed by a ray
    def add_ray(self, start, angle, rng, p):
        # Compute ray final position using ray start, angle and range
        ...

        # Transform intial and final position to cell indexes
        ...

        # Use Bresenham's line algorithm to get traversed cells
        ...

        # Update as no occupied the cells between start and end - 1 and the last cell 
        # with probability p
        ...
