#!/usr/bin/python3

import numpy as np
import rospy
import tf
import tf2_ros
from sensor_msgs.msg import LaserScan
from nav_msgs.msg import OccupancyGrid
from occupancy_gridmap.gridmap import GridMap


class GridmapServer:
    def __init__(self, scan_topic, world_frame_id, cell_size, map_size):
        
        self.world_frame_id = world_frame_id

        # define publishers
        self.occ_grid_pub = ...
        self.scan_sub = ...

        # Initialize TFbuffer and listener
        self.tfBuffer = tf2_ros.Buffer()
        self.listener = tf2_ros.TransformListener(self.tfBuffer)

        # Initialize gridmap and its parameters
        self.gridmap = None 
        self.cell_size = cell_size
        self.map_size = map_size
       
        # Initialize timer to puiblish gridmap
        self.pub_timer = rospy.Timer(rospy.Duration(1.0), self.publish_gridmap)

    # Get a scan and update the gridmap
    def get_scan(self, scan):
        try:
            # Get sensor position and orientation
            trans = self.tfBuffer.lookup_transform(self.world_frame_id, scan.header.frame_id, rospy.Time())
            _, _, yaw = tf.transformations.euler_from_quaternion([trans.transform.rotation.x, 
                                                                  trans.transform.rotation.y,
                                                                  trans.transform.rotation.z,
                                                                  trans.transform.rotation.w])
            # Initialize quadtree with the first measure
            if self.gridmap is None:
                self.gridmap = GridMap(np.array([trans.transform.translation.x, trans.transform.translation.y]), self.cell_size, self.map_size)
        
            # Update the gridmap calling the `add_ray` function for each valid ray.
            init = rospy.Time.now()
            x = ...
            y = ...
            for i, r in enumerate(scan.ranges):
                if r >= scan.range_min and r <= scan.range_max:
                    # Yaw ray must be computed taking into account: sensor yaw, min angle for first ray and angle increment
                    yaw_ray = ...
                    self.gridmap.add_ray(np.array([x, y]), yaw_ray, r, 0.7)
            
        except (tf2_ros.LookupException, tf2_ros.ConnectivityException, tf2_ros.ExtrapolationException):
            print("Invalid TF from {} to {}".format(scan.header.frame_id, self.world_frame_id))
        
    # Publish occupancy gridmap
    def publish_gridmap(self, e):

        if self.gridmap is not None:
            # Get gridmap
            gridmap_logodds = ...
            
            # Transfor "gridmap_logodds" occupancy grid to "data" structure taking into account that:
            # The map data, is in row-major order, starting with (0,0).  Occupancy
            # probabilities are in the range [0,100]. Unknown is -1.
            data = ...

            # Create OccupancyGrid message
            occ_grid = OccupancyGrid()
            occ_grid.header.frame_id = ...
            occ_grid.header.stamp = ...
            occ_grid.info.resolution = ...
            occ_grid.info.width = ...
            occ_grid.info.height = ...
            occ_grid.info.origin.position.x = ...
            occ_grid.info.origin.position.y = ...
            occ_grid.info.origin.position.z = 0
            occ_grid.info.origin.orientation.x = 0
            occ_grid.info.origin.orientation.y = 0
            occ_grid.info.origin.orientation.z = 0
            occ_grid.info.origin.orientation.w = 0
            occ_grid.data = data

            # Publish OccupancyGrid message
            self.occ_grid_pub.publish(occ_grid)
                
            
if __name__ == '__main__':
    # Initialize map server node
    rospy.init_node('gridmap_server')   
    node = GridmapServer('/scan', 'odom', cell_size=0.1, map_size=20)
    
    # Run forever
    rospy.spin()